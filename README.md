# otus-spring
Otus spring course homework (Quiz)

Программа по проведению тестирования студентов:
  - В ресурсах хранятся вопросы и различные ответы к ним в виде
CSV файла (5 вопросов). Программа должна спросить у
пользователя фамилию и имя, спросить 5 вопросов из CSVфайла
и вывести результат тестирования.
  - Все сервисы в программе должны решать строго определённую
задачу. Зависимости должны быть настроены в IoC контейнере.
  - Опционально: сервисы, по возможности, покрыть тестами.
  
Второе задание:  
  
 - Annotation + Java-based конфигурация
 - Добавьте файл настроек 
 - Локализовать выводимые сообщения и вопросы теста.

Для смены локали используйте параметр VM: -Duser.language=ru -Duser.region=RU