package ru.otus.dbarsukov.quiz.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConsoleIO {
    private static final String NUMBER_FORMAT_EXCEPTION_MESSAGE = "Not a number! Enter a number or Ctrl-c to exit.";

    public static Integer readInteger() {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        Integer result = null;

        while (result == null) {
            String inputString = "";
            try {
                inputString = input.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                result = Integer.parseInt(inputString);
            } catch (NumberFormatException e) {
                System.out.println(NUMBER_FORMAT_EXCEPTION_MESSAGE);
            }
        }

        return result;
    }
}
