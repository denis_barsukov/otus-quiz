package ru.otus.dbarsukov.quiz;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.otus.dbarsukov.quiz.config.AppConfig;
import ru.otus.dbarsukov.quiz.domain.Quiz;
import ru.otus.dbarsukov.quiz.service.QuizService;

@Slf4j
public class Main {
    public static void main(String[] args) {
        Thread.setDefaultUncaughtExceptionHandler((t, e) ->
                log.error("Caught: " + e.toString()));

        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(AppConfig.class);

        QuizService service = context.getBean(QuizService.class);
        Quiz quiz = service.getDefault();
        quiz.start();
    }
}
