package ru.otus.dbarsukov.quiz.dao;

import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.MappingStrategy;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import ru.otus.dbarsukov.quiz.domain.QuestionAndAnswers;
import ru.otus.dbarsukov.quiz.dto.QuestionAndAnswersDto;
import ru.otus.dbarsukov.quiz.util.QuestionAndAnswersMapper;

import java.io.Reader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@NoArgsConstructor
@Repository
public class QuestionAndAnswersDaoCsvImpl implements QuestionAndAnswersDao {
    public List<QuestionAndAnswers> getQuestionAndAnswers(String name) {
        Path path;
        try {
            path = Paths.get(ClassLoader.getSystemResource("csv/" + name + ".csv").toURI());
        } catch (URISyntaxException | NullPointerException e) {
            log.error("Can't load quiz file!", e);
            return Collections.emptyList();
        }

        return getList(path);
    }

    private List<QuestionAndAnswers> getList(Path path) {
        try {
            try (Reader reader = Files.newBufferedReader(path)) {
                return convert(new CsvToBeanBuilder(reader)
                        .withType(QuestionAndAnswersDto.class)
                        .withMappingStrategy(getStrategy())
                        .withSeparator(';')
                        .build()
                        .parse());
            }
        } catch (Exception e) {
            log.error("Quiz file parsing error!", e);
            return Collections.emptyList();
        }
    }

    private List<QuestionAndAnswers> convert(List<QuestionAndAnswersDto> dtoList) {
        return dtoList.stream()
                .map(QuestionAndAnswersMapper::convert)
                .collect(Collectors.toList());
    }

    private MappingStrategy getStrategy() {
        HeaderColumnNameMappingStrategy<QuestionAndAnswersDto> strategy = new HeaderColumnNameMappingStrategy<>();
        strategy.setType(QuestionAndAnswersDto.class);
        return strategy;
    }
}
