package ru.otus.dbarsukov.quiz.dao;

import ru.otus.dbarsukov.quiz.domain.QuestionAndAnswers;

import java.util.List;

public interface QuestionAndAnswersDao {
    List<QuestionAndAnswers> getQuestionAndAnswers(String name);
}
