package ru.otus.dbarsukov.quiz.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import ru.otus.dbarsukov.quiz.dao.QuestionAndAnswersDao;
import ru.otus.dbarsukov.quiz.domain.Person;
import ru.otus.dbarsukov.quiz.domain.Quiz;

import java.util.Locale;

@Service
@RequiredArgsConstructor
public class QuizServiceImpl implements QuizService {

    @Qualifier("person")
    private final Person person;
    private final QuestionAndAnswersDao dao;
    private final MessageSource messageSource;

    @Value("${quiz.name:default}")
    private String defaultQuizName = "default";
    @Value("${quiz.threshold:4}")
    private Integer threshold = 1;

    public Quiz getByName(String name) {
        String fullName = name + '_' + Locale.getDefault();
        return new Quiz(person, dao.getQuestionAndAnswers(fullName), messageSource, threshold);
    }

    public Quiz getDefault() {
        return getByName(defaultQuizName);
    }
}

