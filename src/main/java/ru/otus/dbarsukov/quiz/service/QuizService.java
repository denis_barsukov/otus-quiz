package ru.otus.dbarsukov.quiz.service;

import ru.otus.dbarsukov.quiz.domain.Quiz;

public interface QuizService {
    Quiz getByName(String name);
    Quiz getDefault();
}
