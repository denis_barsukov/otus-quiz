package ru.otus.dbarsukov.quiz.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Scanner;

@Component
@Getter
@RequiredArgsConstructor
public class Person {
    private final MessageSource messageSource;

    private String firstName;
    private String surname;

    public void introduceYourself() {
        Scanner scanner = new Scanner(System.in);

        System.out.print(messageSource.getMessage(
                "person.name",
                null,
                Locale.getDefault()));
        firstName = scanner.nextLine();

        System.out.print(messageSource.getMessage(
                "person.surname",
                null,
                Locale.getDefault()));
        surname = scanner.nextLine();
    }
}

