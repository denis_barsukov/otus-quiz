package ru.otus.dbarsukov.quiz.dto;

import com.opencsv.bean.CsvBindByName;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class QuestionAndAnswersDto {
    @CsvBindByName
    private Integer id;
    @CsvBindByName
    private String question;
    @CsvBindByName
    private String answer1;
    @CsvBindByName
    private String answer2;
    @CsvBindByName
    private String answer3;
    @CsvBindByName
    private String answer4;
    @CsvBindByName
    private Integer correctAnswerId;
}
