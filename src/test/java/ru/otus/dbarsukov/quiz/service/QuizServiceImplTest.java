package ru.otus.dbarsukov.quiz.service;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.otus.dbarsukov.quiz.AbstractTest;
import ru.otus.dbarsukov.quiz.config.AppConfig;
import ru.otus.dbarsukov.quiz.dao.QuestionAndAnswersDao;
import ru.otus.dbarsukov.quiz.domain.Person;
import ru.otus.dbarsukov.quiz.domain.QuestionAndAnswers;

import java.io.ByteArrayInputStream;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = AppConfig.class)
class QuizServiceImplTest extends AbstractTest {

    private final String ANSWER = "4" + System.lineSeparator();

    @Mock
    private Person mockPerson;

    @Mock
    private QuestionAndAnswersDao mockDao;

    @Autowired
    private MessageSource messageSource;

    @BeforeEach
    void redirectInput() {
        System.setIn(new ByteArrayInputStream(ANSWER.getBytes()));
    }

    @AfterEach
    void restoreInputAndOutput() {
        System.setIn(defaultSystemIn);
    }

    @Test
    void getByName() {
        doNothing().when(mockPerson).introduceYourself();
        when(mockPerson.getFirstName()).thenReturn(NAME);
        when(mockPerson.getSurname()).thenReturn(SURNAME);

        when(mockDao.getQuestionAndAnswers(anyString()))
                .thenReturn(Collections.singletonList(new QuestionAndAnswers(ID, QUESTION, ANSWERS, CORRECT_ANSWER)));

        QuizService quiz = new QuizServiceImpl(mockPerson, mockDao, messageSource);
        quiz.getByName("baz").start();
    }

}