package ru.otus.dbarsukov.quiz.domain;

import ru.otus.dbarsukov.quiz.AbstractTest;
import ru.otus.dbarsukov.quiz.tags.FastTest;

import static org.junit.jupiter.api.Assertions.*;

class QuestionAndAnswersTest extends AbstractTest {

    @FastTest
    void gettersTest() {
        QuestionAndAnswers questionAndAnswers = new QuestionAndAnswers(ID, QUESTION, ANSWERS, CORRECT_ANSWER);

        assertEquals(ID, questionAndAnswers.getId());
        assertEquals(QUESTION, questionAndAnswers.getQuestion());
        assertEquals(ANSWERS, questionAndAnswers.getAnswers());
        assertEquals(CORRECT_ANSWER, questionAndAnswers.getCorrectAnswerId());
    }

}