package ru.otus.dbarsukov.quiz.domain;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.otus.dbarsukov.quiz.AbstractTest;
import ru.otus.dbarsukov.quiz.config.AppConfig;
import ru.otus.dbarsukov.quiz.tags.FastTest;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = AppConfig.class)
class PersonTest extends AbstractTest {

    @Autowired
    private MessageSource messageSource;

    private final String INPUT = NAME + System.lineSeparator() + SURNAME + System.lineSeparator();
    private final String MESSAGE1 = "Name: ";
    private final String MESSAGE2 = "Surname: ";
    private final String OUTPUT = MESSAGE1 + MESSAGE2;

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @BeforeEach
    void redirectInputAndOutput() {
        System.setIn(new ByteArrayInputStream(INPUT.getBytes()));
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    void restoreInputAndOutput() {
        System.setIn(defaultSystemIn);
        System.setOut(new PrintStream(defaultSystemOut));
    }

    @FastTest
    @DisplayName("Person attributes console input test")
    void introduceYourselfInput() {
        Person person = new Person(messageSource);
        person.introduceYourself();

        assertEquals(NAME, person.getFirstName());
        assertEquals(SURNAME, person.getSurname());
    }

    @FastTest
    @DisplayName("Person class output messages test")
    void introduceYourselfOutput() {
        Person person = new Person(messageSource);
        person.introduceYourself();

        assertEquals(OUTPUT, outContent.toString());
    }
}
