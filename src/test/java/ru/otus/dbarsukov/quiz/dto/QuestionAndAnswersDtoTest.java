package ru.otus.dbarsukov.quiz.dto;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import ru.otus.dbarsukov.quiz.tags.FastTest;

import static com.google.code.beanmatchers.BeanMatchers.*;

class QuestionAndAnswersDtoTest {

    @FastTest
    void testQuestionAndAnswersDto() {
        MatcherAssert.assertThat(QuestionAndAnswersDto.class, CoreMatchers.allOf(
                hasValidBeanConstructor(),
                hasValidGettersAndSetters(),
                hasValidBeanHashCode(),
                hasValidBeanEquals(),
                hasValidBeanToString()
        ));
    }
}