package ru.otus.dbarsukov.quiz.util;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import ru.otus.dbarsukov.quiz.tags.FastTest;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ru.otus.dbarsukov.quiz.util.ConsoleIO.readInteger;

class ConsoleIOTest {

    private static final String NUMBER = "123";
    private static final String INCORRECT_INPUT = "not integer string" + System.lineSeparator() + NUMBER;
    private static final String OUTPUT = "Not a number! Enter a number or Ctrl-c to exit." + System.lineSeparator();

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    private final InputStream defaultSystemIn = System.in;
    private final PrintStream defaultSystemOut = System.out;

    @BeforeEach
    void redirectInputAndOutput() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    void restoreInputAndOutput() {
        System.setIn(defaultSystemIn);
        System.setOut(new PrintStream(defaultSystemOut));
    }

    @FastTest
    @DisplayName("Read correct integer value from console")
    void readInt() {
        System.setIn(new ByteArrayInputStream(NUMBER.getBytes()));
        int number = readInteger();

        assertEquals(123, number);
    }

    @FastTest
    @DisplayName("Read incorrect integer value from console")
    void readNotInt() {
        System.setIn(new ByteArrayInputStream(INCORRECT_INPUT.getBytes()));
        readInteger();

        assertEquals(OUTPUT, outContent.toString());
    }

}