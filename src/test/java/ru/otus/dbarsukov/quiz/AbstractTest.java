package ru.otus.dbarsukov.quiz;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class AbstractTest {
    protected final String NAME = "foo";
    protected final String SURNAME = "bar";

    protected final Integer ID = 1;
    protected final String QUESTION = "?";
    protected final List<String> ANSWERS = Arrays.asList("a", "b", "c", "d");
    protected final Integer CORRECT_ANSWER = 4;

    protected final InputStream defaultSystemIn = System.in;
    protected final PrintStream defaultSystemOut = System.out;

    private static final Locale defaultLocale = Locale.getDefault();
    private static final Locale enLocale = new Locale("en", "EN");

    @BeforeAll
    static void setEnLocale() {
        Locale.setDefault(enLocale);
    }

    @AfterAll
    static void setDefaultLocale() {
        Locale.setDefault(defaultLocale);
    }
}
