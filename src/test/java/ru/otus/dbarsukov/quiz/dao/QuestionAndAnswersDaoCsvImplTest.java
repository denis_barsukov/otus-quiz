package ru.otus.dbarsukov.quiz.dao;

import org.apache.log4j.Appender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.otus.dbarsukov.quiz.AbstractTest;
import ru.otus.dbarsukov.quiz.domain.QuestionAndAnswers;
import ru.otus.dbarsukov.quiz.tags.FastTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class QuestionAndAnswersDaoCsvImplTest extends AbstractTest {

    @Mock
    private Appender mockAppender;

    @Test
    void noSuchCsvFileLoggingTest() {
        Logger logger = Logger.getLogger(QuestionAndAnswersDaoCsvImpl.class);
        logger.addAppender(mockAppender);

        ArgumentCaptor<LoggingEvent> eventArgumentCaptor = ArgumentCaptor.forClass(LoggingEvent.class);

        QuestionAndAnswersDao dao = new QuestionAndAnswersDaoCsvImpl();
        dao.getQuestionAndAnswers("no_such_file");

        verify(mockAppender, times(1)).doAppend(eventArgumentCaptor.capture());

        assertEquals("Can't load quiz file!", eventArgumentCaptor.getAllValues().get(0).getMessage());
        assertEquals(Level.ERROR, eventArgumentCaptor.getAllValues().get(0).getLevel());
    }

    @FastTest
    void getQuestionAndAnswers() {
        QuestionAndAnswersDao dao = new QuestionAndAnswersDaoCsvImpl();
        List<QuestionAndAnswers> questionAndAnswers = dao.getQuestionAndAnswers("test_en_EN");

        assertEquals(1, questionAndAnswers.size());
        assertEquals(ID, questionAndAnswers.get(0).getId());
        assertEquals(QUESTION, questionAndAnswers.get(0).getQuestion());
        assertEquals(ANSWERS, questionAndAnswers.get(0).getAnswers());
        assertEquals(CORRECT_ANSWER, questionAndAnswers.get(0).getCorrectAnswerId());
    }

    @FastTest
    void wrongCsvFormat() {
        Logger logger = Logger.getLogger(QuestionAndAnswersDaoCsvImpl.class);
        logger.addAppender(mockAppender);

        ArgumentCaptor<LoggingEvent> eventArgumentCaptor = ArgumentCaptor.forClass(LoggingEvent.class);

        QuestionAndAnswersDao dao = new QuestionAndAnswersDaoCsvImpl();
        List<QuestionAndAnswers> questionAndAnswers = dao.getQuestionAndAnswers("wrongFormat_en_EN");

        verify(mockAppender, times(1)).doAppend(eventArgumentCaptor.capture());

        assertEquals("Quiz file parsing error!", eventArgumentCaptor.getAllValues().get(0).getMessage());
        assertEquals(Level.ERROR, eventArgumentCaptor.getAllValues().get(0).getLevel());

        assertEquals(true, questionAndAnswers.isEmpty());
    }

    @FastTest
    void emptyCsv() {
        QuestionAndAnswersDao dao = new QuestionAndAnswersDaoCsvImpl();
        List<QuestionAndAnswers> questionAndAnswers = dao.getQuestionAndAnswers("empty");

        assertEquals(true, questionAndAnswers.isEmpty());
    }

}